package main

import (
	"fmt"
	"net/http"
	"os"
    "encoding/json"
)

type Article struct {
	Title string `json:"title"`
	Content string `json:"content"`
}

func getTreatmentHandler(w http.ResponseWriter, r *http.Request) {
	var response = Article {
		Title: "Thyroidectomy",
		Content: "Removing all or part of your thyroid gland (total or partial thyroidectomy) " +
		"is an option if you have a large goiter that is uncomfortable or causes difficulty breathing " + 
		"or swallowing, or in some cases, if you have a nodular goiter causing hyperthyroidism. Surgery  " +
		"is also the treatment for thyroid cancer.",
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Servicing root")
	var response = Article {
		Title: "Root",
		Content: "This the root",
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func listenAndServe(port string) {
	fmt.Printf("serving on %s\n", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}

func main() {
	http.HandleFunc("/", helloHandler)
	http.HandleFunc("/treatment", getTreatmentHandler)
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "8080"
	}
	go listenAndServe(port)
	select {}
}
